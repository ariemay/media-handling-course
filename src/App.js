import React, { Component } from 'react';
import { PrismCode } from 'react-prism'
import { Player, ControlBar } from "video-react"
import { Button } from "reactstrap"
import { Page, Text, View, Document, StyleSheet, BlobProvider } from "@react-pdf/renderer"
import './App.css';

const styles = StyleSheet.create({
  page: {
    backgroundColor: "#ebebeb"
  },
  title: {
    fontSize: "16pt"
  },
  section: {
    margin: 4, padding: 8, fontSize: "12pt"
  }
})

const sources = {
  sintelTrailer: "http://media.w3.org/2010/05/sintel/trailer.mp4",
  bunnyTrailer: "http://media.w3.org/2010/05/bunny/trailer.mp4",
  bunnyMovie: "http://media.w3.org/2010/05/bunny/movie.mp4",
  test: "http://media.w3.org/2010/05/video/movie_300.webm"
}

function pdfText() {
  return (
    <Document>
      <Page size={"A4"} style={styles.page}>
        <View style={styles.section}>
          <Text style={styles.title}>
          Parodi Hujan
          </Text>
          <View style={styles.section}>
            <Text>Test a ada lebih basah</Text>
            <Text>Bebas ajah</Text>
          </View>
        </View>
      </Page>
    </Document>
  )
}

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      source: sources.bunnyMovie
    }

    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.load = this.load.bind(this);
    this.changeCurrentTime = this.changeCurrentTime.bind(this)
    this.seek = this.seek.bind(this)
    this.changePlaybackRateRate = this.changePlaybackRateRate.bind(this)
    this.changeVolume = this.changeVolume.bind(this)
    this.setMuted = this.setMuted.bind(this)
  }

  // componentDidMount() {
  //   this.player.subscribeToStateChange(this.handleStateChange.bind(this));
  // }

  setMuted(muted) {
    return () => {
      this.player.muted = muted;
    }
  }

  handleStateChange(state) {
    this.setState({
      player: state
    });
  }

  play() {
    this.player.play()
  }

  pause() {
    this.player.pause();
  }

  load() {
    this.player.load()
  }

  changeCurrentTime(seconds) {
    return () => {
      const { player } = this.player.getState();
      this.player.seek(player.currenTime + seconds);
    }
  }

  seek(seconds) {
    return () => {
      this.player.seek(seconds)
    }
  }

  changePlaybackRateRate(steps) {
    return () => {
      const { player } = this.player.getState()
      this.player.playbackRate = player.playbackRate + steps
    }
  }

  changeVolume(steps) {
    return () => {
      const { player } = this.player.getState();
      this.player.volume = player.volume + steps;
    }
  }

  changeSource(name) {
    return () => {
      this.setState({
        source: sources[name]
      })
      this.player.load();
    }
  }

  render() {
    return (
      <div style={{ height: "100%" }}>
        <BlobProvider document={pdfText()}>
          {({ url }) => <iframe src={url} style={{
            width: "100%",
            height: "100%"
          }} />}
        </BlobProvider>
      </div>
    )
  //   return (
  //     <div>
  //       <Player ref={player => {
  //         this.player = player;
  //       }}
  //       autoPlay>
  //         <source src={this.state.source}/>
  //         <ControlBar autoHide={false}/>
  //       </Player>
  //       <div className='py-3'>
  //         <Button onClick={this.play} className='mr-3'>
  //           play()
  //         </Button>
  //         <Button onClick={this.pause} className='mr-3'>
  //           pause()
  //         </Button>
  //         <Button onClick={this.load} className='mr-3'>
  //           load()
  //         </Button>
  //         <div className='pb-3'>
  //           <Button onClick={this.changeSource("test")} className='mr-3'>
  //             Test Movie
  //           </Button>
  //         </div>
  //       </div>
  //     </div>
  //   );
  }
}

export default App;
